#!/usr/bin/env bash
# autoupdate scripts in this directory
# it simply executed the suggested update commands written in readme.md

set -x

export SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )


## refresh image 
docker pull quay.io/fravi/ci-imagebuilder:master
OWNERID=$(stat -c '%u' ./)

## clean ci directory 
if [ -d ${PROJECT_DIR}/ci ]; then
  rm -f ${PROJECT_DIR}/ci/*
fi

docker run --rm -ti -e OWNERID=${OWNERID} -v ${PROJECT_DIR}/ci:/target quay.io/fravi/ci-imagebuilder:master


