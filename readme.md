tag v0.1


# to move tag to current commit ( also on remote origin ) 
```sh
tagname="v0.1"
git push origin :refs/tags/$tagname
# Replace the tag to reference the most recent commit
git tag -fa $tagname -m 'tag'
# Push the tag to the remote origin
git push origin master --tags
```